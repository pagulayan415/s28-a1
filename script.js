// Get all todos
// fetch('https://jsonplaceholder.typicode.com/todos')
// .then((response) => response.json())
// .then((data) => {
// 	let list = data.map((todo) => {
// 	return todo.title;
// 	})
// 	console.log(list)
// })


// Getting a specific to do list item
// fetch('https://jsonplaceholder.typicode.com/todos/1')
// .then((response) => response.json())
// .then((data) => console.log(`The item "${data.title}" has a status of ${data.completed}`))


// Creating a todo list item using POST method
// fetch('https://jsonplaceholder.typicode.com/todos', {
// 	method: 'POST',
// 	headers: {
// 		'Content-type': 'application/json'
// 	},
// 	body:JSON.stringify({
// 	title: 'Created T Do List Item',
// 	completed: false,
// 	userId: 1
// 	})
// })

// .then((response) => response.json())
// .then((data) => console.log(data))

// Updating a to do list item using PUT method
// fetch('https://jsonplaceholder.typicode.com/todos/1', {
// 	method: 'PUT',
// 	headers: {
// 		'Content-type': 'application/json'
// 	},
// 	body:JSON.stringify({
// 	title: 'Updated To Do List Item',
// 	description: 'To update the my to do list with a different data structure',
// 	status: 'pending',
// 	dateCompleted: 'Pending',
// 	userId: 1
// 	})
// })
// .then((response) => response.json())
// .then((data) => console.log(data))

// Updating a todo list item using PATCH method
// fetch('https://jsonplaceholder.typicode.com/todos/1', {
// 	method: 'PATCH',
// 	headers: {
// 		'Content-type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		status: 'Completed',
// 		dataCompleted: '01/19/22'
// 	})
// })
// .then((response)=> response.json())
// .then((data) => console.log(data))

// Deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE'
})
.then((response)=> response.json())
.then((data) => console.log(data))

